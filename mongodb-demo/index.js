const mongoose = require('mongoose')
const _ = require('underscore')

const DB_NAME = 'mongodb-demo'

// noinspection JSUnusedGlobalSymbols
const sampleSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'The name must not be blank.'],
    minlength: 5,
    maxlength: 50,
    match: [
      /^[a-zA-Z0-9]([a-zA-Z0-9]*(\s[a-zA-Z0-9])|[a-zA-Z0-9])*$/,
      'The name should contain only letters and numbers. There may be a single space between them.'
    ]
  },
  category: {
    type: String,
    required: true,
    enum: ['default', 'development', 'production', 'test']
  },
  author: String,
  tags: {
    type: [String],
    validate: {
      isAsync: true,
      validator: function (v, callback) {
        setTimeout(() => {
          const result = v && v.length > 0
          console.log(`Async tag validation result is: ${result}`)
          callback(result)
        }, _.random(10, 100))
      },
      message: 'At least one tag should be entered.'
    }
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: Date,
  value: {
    type: Number,
    required: function () {
      return this.isPublished
    }
  },
  isPublished: Boolean
})

// noinspection JSUnresolvedFunction
const Sample = mongoose.model('Sample', sampleSchema)

main().catch(reason => {
  const errors = reason.errors
  if (errors) {
    for (let e in errors) {
      console.error(errors[e].message)
    }
  } else {
    console.error(reason.message)
  }
})

async function main () {
  await connect()

  // Clear the collection and re-create
  await rebuildSamples()

  // RegEx, or, limit, sort, select
  await findSamplesWithRegex()

  // Number of samples in the collection
  await countSamples()

  // Find samples page by page
  await countSamplePages(3, 20)

  // Update with "Query First" approach
  await queryFirstUpdate()

  // Update with "Update First" approach
  await updateFirstUpdate()

  // Delete specific sample
  await deleteTheSmallest()

  await disconnect()
}

/**
 * Connect to the database
 * @returns {Promise<void>}
 */
async function connect () {
  await mongoose
    .connect(`mongodb://localhost/${DB_NAME}`, { useNewUrlParser: true })
    .then(value => console.log(`Connected to "${value.connections[0].name}".`))
}

/**
 * Disconnect from the database
 * @returns {Promise<void>}
 */
async function disconnect () {
  await mongoose
    .disconnect()
    .then(() => console.log('Disconnected from the database.'))
}

/**
 * Deletes all samples in the samples collection
 * @returns {Promise<Query<DeleteWriteOpResultObject["result"]> & {}>}
 */
async function deleteSamples () {
  return Sample.deleteMany({})
}

/**
 * Creates many samples and saves them to the collection
 * @param numSamples {number} Number of samples to create
 * @returns {Promise<void>}
 */
async function createSamples (numSamples) {
  const categoryValues = Sample.schema.path('category').enumValues
  for (let i = 0; i < numSamples; i++) {
    const sample = new Sample({
      name: 'sample0',
      category: categoryValues[_.random(categoryValues.length - 1)],
      author: 'Sample Writer',
      tags: ['sample', 'new', 'test'],
      value: Math.random() * 100,
      isPublished: Math.random() < 0.5
    })

    await sample.save()
  }
}

/**
 * Clear the collection and re-create
 * @returns {Promise<void>}
 */
async function rebuildSamples () {
  await deleteSamples()
  await createSamples(50)
}

async function findSamplesWithRegex () {
  await Sample
    .find({ name: /^sample/ })
    .or([{ name: /.*sample.*/i }, { author: /writer$/i }])
    .and([{ isPublished: true }, { value: { $gte: 20, $lte: 80 } }])
    .limit(10)
    .sort({ value: -1 })
    .select({ value: 1 })
    .then(console.log)
}

async function countSamples () {
  await Sample
    .countDocuments()
    .then(value => console.log(`There are ${value} records in total.`))
}

/**
 * Split the collection into pages and count
 * @param numPages Total number of pages
 * @param pageSize The number of samples on a page
 * @returns {Promise<void>}
 */
async function countSamplePages (numPages, pageSize) {
  return Sample
    .countDocuments()
    .skip((numPages - 1) * pageSize)
    .limit(pageSize)
    .then(value => console.log(`There are ${value} records in page ${numPages} with ${pageSize} page size.`))
}

/**
 * Makes a query with "Query First" approach and updates appropriate sample
 * @returns {Promise<void>}
 */
async function queryFirstUpdate () {
  const id = (
    await Sample
      .findOne()
      .then()
  ).id

  const newSample = {
    name: 'Query First',
    author: 'Updater',
    tags: ['updated', 'query first', 'sample'],
    updatedAt: Date.now(),
    value: Math.random() * 100,
    isPublished: Math.random() < 0.5
  }

  const _sample = await Sample.findById(id)

  for (let k in newSample) {
    // noinspection JSUnfilteredForInLoop
    _sample[k] = newSample[k]
  }
  _sample.increment()

  await _sample.validate()
  await _sample.save(newSample)
}

/**
 * Makes a query with "Update First" approach and updates appropriate sample
 * @returns {Promise<void>}
 */
async function updateFirstUpdate () {
  const id = (
    await Sample
      .find()
      .sort('id')
      .limit(2)
      .then()
  )[1]._id

  await Sample.updateOne({ _id: id }, {
    $inc: {
      __v: 1
    },
    $set: {
      name: 'Update First',
      author: 'Updater',
      tags: ['updated', 'update first', 'sample'],
      updatedAt: Date.now(),
      value: Math.random() * 100,
      isPublished: Math.random() < 0.5
    }
  }, {
    runValidators: true
  })
}

/**
 * Deletes the sample with the smallest value
 * @returns {Promise<void>}
 */
async function deleteTheSmallest () {
  const sample = (
    await Sample
      .findOne()
      .sort('value')
  )

  await Sample.deleteOne({ _id: sample.id })
    .then(console.log(`Deleted sample with the smallest value: ${sample}`))
}
