const _ = require('underscore')

// Promise.resolve({ id: 1 }).then(console.log)
// Promise.reject(new Error()).catch(console.log)

const promises = []

for (let i = 0; i < 10; i++) {
  promises.push(new Promise((resolve, reject) => {
    setTimeout(() => {
      const id = `Promise ${i}`

      if (Math.random() < 0.5) {
        resolve(id)
      } else {
        reject(new Error(id))
      }
    }, getTimeout())
  }))
}

Promise.all(promises)
  .then(console.log)
  .catch(console.log)

promises.length = 0

for (let i = 0; i < 10; i++) {
  promises.push(new Promise(resolve => setTimeout(() => resolve(`Promise ${i}`), getTimeout())))
}

Promise.all(promises)
  .then(console.log)

Promise.race(promises)
  .then(promise => console.log(`The winner is ${promise}`))

/**
 *
 * @returns {number} Random integer value as timeout
 */
function getTimeout () {
  return _.random(500, 2000)
}
