const p = new Promise((resolve, reject) => {
  setTimeout(() => {
    if (Math.random() < 0.5) {
      resolve(200)
    } else {
      reject(new Error('error'))
    }
  }, 1000)
})

p
  .then(value => console.log(value))
  .catch(reason => console.log(reason.message))
