const _ = require('underscore')

class Data {
  /**
   *
   * @param id {number}
   * @param details {*?}
   */
  constructor (id, details) {
    this.id = id
    this.details = details
  }
}

class User {
  /**
   *
   * @param id {number}
   * @param username {string}
   * @param dataset {Iterable<Data>?}
   */
  constructor (id, username, dataset) {
    this.id = id
    this.username = username
    this.dataset = dataset
  }
}

console.log('before the async call')

connectUser().then(console.log).catch(console.error)

console.log('after the async call')

async function connectUser () {
  try {
    const user = await getUser(10)
    await getUserData(user)
    return getUserDataDetails(user)
  } catch (e) {
    throw e
  }
}

function getUser (id) {
  return new Promise((resolve, reject) => {
    if (Math.random() < 0.75) {
      setTimeout(() => resolve(new User(id, 'test')), getTimeout())
    } else {
      setTimeout(() => reject(new Error('Couldn\'t find the user with given ID.')), getTimeout())
    }
  })
}

function getUserData (user) {
  return new Promise((resolve, reject) => {
    if (Math.random() < 0.75) {
      const dataset = []
      for (let i = 0; i < 10; i++) {
        dataset.push(new Data(i))
      }
      user.dataset = dataset
      setTimeout(() => resolve(user), getTimeout())
    } else {
      setTimeout(() => reject(new Error('Couldn\'t find any data about the requested user.')), getTimeout())
    }
  })
}

function getUserDataDetails (user) {
  return new Promise((resolve, reject) => {
    if (Math.random() < 0.75) {
      const data = user.dataset[0]
      data.details = { title: 'Test Data', content: 'This is a test data' }
      setTimeout(() => resolve(user), getTimeout())
    } else {
      setTimeout(() => reject(new Error('Couldn\'t find any details about the requested data')), getTimeout())
    }
  })
}

/**
 *
 * @returns {number} Random integer value as timeout
 */
function getTimeout () {
  return _.random(100, 1000)
}
