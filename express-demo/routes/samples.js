const Joi = require('joi')
const express = require('express')

const router = express.Router()

class Sample {
  /**
   *
   * @param {number} id
   * @param {string} name
   */
  constructor (id, name) {
    this._id = id
    this.name = name
  }

  get id () {
    return this._id
  }
}

class SampleResult {
  /**
   *
   * @param {Sample} sample
   * @param {number} index
   */
  constructor (sample, index) {
    this.sample = sample
    this.index = index
  }
}

const samples = []
for (let i = 0; i < 10; i++) {
  const sample = new Sample(i, `sample${i}`)
  samples.push(sample)
}

router.get('', (_req, res) => res.send(samples))
router.get('/:id', (req, res) => {
  const {
    sample
  } = findSampleWithNotFoundResponse(req.params.id, res)
  if (!sample) return

  res.send(sample.name)
})
router.get('/:year/:month', (req, res) => res.send(req.params))
router.get('/:year/:month/:day', (req, res) => {
  res.send({
    params: req.params,
    query: req.query
  })
})

router.post('', (req, res) => {
  if (validateSampleWithBadRequestResponse(req.body, res)) return

  const sample = {
    id: samples.length,
    name: req.body.name
  }
  samples.push(sample)
  res.send(sample)
})

router.put('/:id', (req, res) => {
  const {
    sample
  } = findSampleWithNotFoundResponse(req.params.id, res)
  if (!sample) return

  if (validateSampleWithBadRequestResponse(req.body, res)) return

  sample.name = req.body.name
  res.send(sample.name)
})

router.delete('/:id', (req, res) => {
  const result = findSampleWithNotFoundResponse(req.params.id, res)
  if (!result.sample) return

  samples.splice(result.index, 1)
  res.send(result.sample)
})

/**
 * Finds the sample if exists, sends the not found response otherwise.
 * @param {string} id Position of the sample
 * @param {Response} response HTTP response
 * @returns {SampleResult} An object with sample and index properties
 */
function findSampleWithNotFoundResponse (id, response) {
  const index = samples.findIndex(c => c.id.toString() === id)
  if (index < 0) {
    response.status(404).send('There is no sample with the given ID.')
    return new SampleResult(undefined, undefined)
  }

  const sample = samples[index]
  return new SampleResult(sample, index)
}

/**
 * Validates the requested sample object and sends the bad request response
 * if not appropriate
 * @param {Sample} sample The sample object to be validated
 * @param {Response} response HTTP response
 * @returns {?string} null if valid, error message otherwise
 */
function validateSampleWithBadRequestResponse (sample, response) {
  const schema = {
    name: Joi.string().min(3).required()
  }
  const validate = Joi.validate(sample, schema)
  if (validate.error) {
    const message = validate.error.details[0].message
    response.status(400).send(message)
    return message
  }

  return null
}

module.exports = router
