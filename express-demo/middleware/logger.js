/**
 * Prints to the console
 * @param {Request} request
 * @param {Response} response
 * @param {function} next
 */
function log (request, response, next) {
  console.log(request)
  console.log(response)

  next()
}

module.exports = log
