const startupDebugger = require('debug')('app:startup')
const dbDebugger = require('debug')('app:db')
const express = require('express')
const helmet = require('helmet')
const morgan = require('morgan')
const config = require('config')
const home = require('./routes/home')
const samples = require('./routes/samples')

const port = process.env.PORT || 3000
const app = express()

const ENVS = {
  DEVELOPMENT: 'development'
}

app.set('view engine', 'pug')
// app.set('views', './views') // default value

console.log(`Application Name: ${config.get('name')}`)
console.log(`Mail Server: ${config.get('mail.host')}`)
if (config.has('mail.password')) console.log(`Mail Password: ${config.get('mail.password')}`)
console.log(`Author: ${config.get('author')}`)

console.log(`NODE_ENV: ${process.env.NODE_ENV}`)
console.log(`app: ${app.get('env')}`)

app.use(helmet())
app.use(express.json())
app.use(express.urlencoded({
  extended: true
}))
app.use(express.static('public'))
app.use('/', home)
app.use('/samples/', samples)

if (app.get('env') === ENVS.DEVELOPMENT) {
  const option = 'tiny'
  app.use(morgan(option))
  startupDebugger(`Morgan enabled with ${option} option.`)
}

dbDebugger('Connected to database.')

app.listen(port, () => console.log(`Listenin on port ${port}...`))
